#include <vector>
#include <string>
#include <iostream>

#ifndef MATRIX_H
#define MATRIX_H

class Matrix
{
public:
    Matrix(unsigned rows, unsigned cols);
    double get(unsigned i, unsigned j);
    void set(unsigned i, unsigned j, double value);
    unsigned getWidth();
    unsigned getHeight();
    std::string print();

private:
    unsigned rows_;
    unsigned columns_;
    std::vector<double> data_;

};

#endif
