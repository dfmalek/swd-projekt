#include "Matrix.h"


Matrix::Matrix(unsigned rows, unsigned columns):
    rows_(rows),
    columns_(columns),
    data_(rows * columns)
{

}

unsigned Matrix::getWidth()
{
    return columns_;
}

unsigned Matrix::getHeight()
{
    return rows_;
}

double Matrix::get(unsigned i, unsigned j)
{
      return data_[i * columns_ + j];
}

void Matrix::set(unsigned i, unsigned j, double value)
{
      data_[i * columns_ + j] = value;
}

std::string Matrix::print()
{
    std::string result;
    for(int row = 0; row < rows_; row++)
    {
        for(int col = 0; col < columns_; col++)
        {
            std::cout << data_[row * columns_ + col] << " ";
        }
        std::cout << std::endl;
    }
    return result;
}

