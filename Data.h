#include "Matrix.h"

#ifndef DATA_H
#define DATA_H

struct Decision
{
    Decision(unsigned packet,unsigned processor, unsigned connection) :
        packetId(packet), processorId(processor), connectionId(connection)
    {
    }

    unsigned packetId;
    unsigned processorId;
    unsigned connectionId;
};

typedef std::vector<Decision> Decisions;

struct Connection
{
    unsigned maxCapacity;
    unsigned capacityTaken;
};

struct Processor
{
    unsigned id;
    unsigned maxCapacity;
    unsigned capacityTaken;
    unsigned assignedPackets;
    unsigned maxPackets;
};

struct Packet
{
    unsigned id;
    unsigned size;
};


//packetId
//PacketSize

struct Data
{
    std::vector<Packet> packets;
    std::vector<Processor> processors;
    std::vector<Connection> connections;
    std::vector<Matrix> A;
};

#endif
