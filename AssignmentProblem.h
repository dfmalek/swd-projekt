#include <string>
#include <vector>

#include "Data.h"

#ifndef ASSIGNMENT_PROBLEM_H
#define ASSIGNMENT_PROBLEM_H

class AssignmentProblem
{
public:
    AssignmentProblem();

    //void readData(std::string path);
    Matrix solve(const Data& data);


    
private:
    void updateData(const Decision& decision, Matrix& assignmentResult);

    Decisions findPossibleDecisions(const unsigned& packetId);
    Decisions checkCableCapacity(const std::vector<Decision>& decisions);
    Decisions checkProcessorCapacity(const std::vector<Decision>& decisions);
    Decisions checkProcessorPacketsCapacity(const std::vector<Decision>& decisions);
    Decisions filterDecisionsUsingMostProcessors(const std::vector<Decision>& decisions);
    Decisions filterMostBalancedDecisions(const std::vector<Decision>& decisions);

    Data data;
};

#endif
