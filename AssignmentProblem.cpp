#include "AssignmentProblem.h"
#include <iostream>
#include <map>
#include <algorithm>

AssignmentProblem::AssignmentProblem()
{

}

Matrix AssignmentProblem::solve(const Data& data)
{
    this->data=data;
    //std::cout << data.A.size() << "  " << data.connections.size() << "  " << data.packets.size() << "  " << data.processors.size() << std::endl;

    Matrix result(data.packets.size(), data.processors.size());

    std::vector<Packet> sortedPackets(data.packets.begin(), data.packets.end());
    std::sort(sortedPackets.begin(), sortedPackets.end(), [&](Packet lhs, Packet rhs)
        {
            return lhs.size > rhs.size;
        });

    for (auto packet: sortedPackets)
    {
        std::cout << packet.id << std::endl;
    }

    for (auto& packet : sortedPackets)
    {
        std::cout << "Possible decisions for packet " << packet.id << " : " << std::endl;
        auto possibleDecisions = findPossibleDecisions(packet.id);
        std::cout << "Possible Decisions size : " << possibleDecisions.size() << std::endl;
        possibleDecisions = checkCableCapacity(possibleDecisions);
        std::cout << "CAble Capacity size : " << possibleDecisions.size() << std::endl;
        possibleDecisions = checkProcessorCapacity(possibleDecisions);
        std::cout << "Processor Capacity size : " << possibleDecisions.size() << std::endl;
        possibleDecisions = checkProcessorPacketsCapacity(possibleDecisions);
        std::cout << "Processor Packet Capacity size : " << possibleDecisions.size() << std::endl;
        possibleDecisions = filterDecisionsUsingMostProcessors(possibleDecisions);
        std::cout << "MostCPUs decisions size : " << possibleDecisions.size() << std::endl;
        possibleDecisions = filterMostBalancedDecisions(possibleDecisions);
        std::cout << "Most balanced decisions size : " << possibleDecisions.size() << std::endl;
        if (!possibleDecisions.empty())
            updateData(possibleDecisions.front(), result);
        //else
            //std::cout << "packet : " << packet.id << " was not assigned!" << std::endl;


        //return decision;

    //}
    //return Matrix(1, 1);
    }
    return result;
}

void AssignmentProblem::updateData(const Decision& decision, Matrix& assignmentResult)
{
    std::cout << "Assigned packet : " << decision.packetId << " to processor : " << decision.processorId << " using connection Id : " << decision.connectionId << std::endl << std::endl;

    assignmentResult.set(decision.packetId, decision.processorId, 1);
    data.connections[decision.connectionId].capacityTaken += data.packets[decision.packetId].size;
    data.processors[decision.processorId].capacityTaken += data.packets[decision.packetId].size;
    data.processors[decision.processorId].assignedPackets++;
            //capacityTaken;
              //  unsigned assignedPackets;
}

Decisions AssignmentProblem::findPossibleDecisions(const unsigned& packetId)
{
    std::vector<Decision> possibleDecisions;
    unsigned connectionId = 0;
    for (auto aN : data.A)
    {
        for (unsigned processorId = 0; processorId < aN.getWidth(); processorId++)
        {
            if (aN.get(packetId, processorId) == 1)
            {
                //std::cout << "decision : " << packetId << "  " << processorId << "  " << connectionId << std::endl;
                possibleDecisions.push_back(Decision(packetId, processorId, connectionId));
            }
        }
        connectionId++;
    }

    return possibleDecisions;
}

Decisions AssignmentProblem::checkCableCapacity(const std::vector<Decision>& decisions)
{
    Decisions result; // should be used only 1 connection per packet
    for (const auto& decision : decisions)
    {
        auto connection = data.connections[decision.connectionId];
        //std:: cout << connection.capacityTaken << "  " <<  data.packets[decision.packetId].size << "  " << connection.maxCapacity << std::endl;
        if (connection.capacityTaken + data.packets[decision.packetId].size <= connection.maxCapacity)
        {
            //std::cout << "pushing decision : " << decision.packetId << "  " << decision.processorId << "  " << decision.connectionId << std::endl;
            result.push_back(decision);
        }
    }

    return result;
}

Decisions AssignmentProblem::checkProcessorCapacity(const std::vector<Decision>& decisions)
{
    Decisions result;

    for (const auto& decision : decisions)
    {
        auto processor = data.processors[decision.processorId];
        //std::cout << processor.capacityTaken + data.packets[decision.packetId].size << "   " << processor.maxCapacity << std::endl;
        if (processor.capacityTaken + data.packets[decision.packetId].size <= processor.maxCapacity)
            result.push_back(decision);
    }

    return result;
}

Decisions AssignmentProblem::checkProcessorPacketsCapacity(const std::vector<Decision>& decisions)
{
    Decisions result;

    for (const auto& decision : decisions)
    {
        auto processor = data.processors[decision.processorId];
        if (processor.assignedPackets < processor.maxPackets)
            result.push_back(decision);
    }

    return result;
}

Decisions AssignmentProblem::filterDecisionsUsingMostProcessors(const std::vector<Decision>& decisions)
{
    if(decisions.size() == 1 || decisions.empty()) return decisions;

    std::vector<std::pair<Decision, unsigned>> usedProcessors;
    unsigned maxValue = 0;

    for (const auto& decision : decisions)
    {
        unsigned value = std::count_if(data.processors.begin(), data.processors.end(),[&] (Processor processor)
            {return processor.assignedPackets > 0 || processor.id == decision.processorId;});
        //std::cout << "value of maxCPU : "<< value;
        if (value > maxValue) maxValue = value;
        usedProcessors.push_back(std::pair<Decision, unsigned>(decision, value));
    }

    Decisions result;
    for(const auto& element : usedProcessors)
    {
        if (element.second == maxValue) result.push_back(element.first);
    }

    return result;
}


Decisions AssignmentProblem::filterMostBalancedDecisions(const std::vector<Decision>& decisions)
{
    if(decisions.size() == 1 || decisions.empty()) return decisions;

    std::vector<std::pair<Decision, unsigned>> capacityTaken;
    unsigned minValue = data.packets.at(decisions.front().packetId).size + data.processors[decisions.front().processorId].capacityTaken;

    std::cout <<  "min value : " << minValue << std::endl;
    for (const auto& decision : decisions)
    {
        unsigned value = data.packets.at(decision.packetId).size + data.processors[decision.processorId].capacityTaken;
        if (value < minValue) minValue = value;
        capacityTaken.push_back(std::pair<Decision, unsigned>(decision, value));
    }

    Decisions result;
    std::cout <<  "min value : " << minValue << std::endl;
    for(const auto& element : capacityTaken)
    {
        std::cout << "trololol " << element.second << std::endl;
        if (element.second == minValue) result.push_back(element.first);
    }

    return result;

    return decisions;
}
