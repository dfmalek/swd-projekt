#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "AssignmentProblem.h"

std::vector<Packet> readPackets(const std::string& input)
{
    std::vector<Packet> packets;
    std::stringstream stream(input);

    unsigned i = 0;
    while(1)
    {
       unsigned packetSize;
       stream >> packetSize;
       if(!stream)
          break;
       packets.push_back({i, packetSize});
       i++;
    }

    return packets;
}

std::vector<Processor> readProcessorsCapacities(const std::string& input)
{
    std::vector<Processor> result;

    std::stringstream stream(input);
    unsigned i = 0;
    while(1)
    {
       unsigned processorSize;
       stream >> processorSize;
       if(!stream)
          break;
       result.push_back({i, processorSize, 0, 0, 0});
       i++;
    }

    return result;
}

void updateProcessorsMaxPackets(const std::string& input, std::vector<Processor>& processors)
{
    std::vector<Processor> result;

    std::stringstream stream(input);
    unsigned i = 0;
    while(1)
    {
       unsigned maxPackets;
       stream >> maxPackets;
       if(!stream)
          break;
       processors[i].maxPackets = maxPackets;
       i++;
    }
}

std::vector<Connection> readConnections(const std::string& input)
{
    std::vector<Connection> result;

    std::stringstream stream(input);
    while(1)
    {
       unsigned connectionCapacity;
       stream >> connectionCapacity;
       if(!stream)
          break;
       result.push_back({connectionCapacity, 0});
    }

    return result;
}

Matrix readPossibleConnections(std::vector<std::string> input, unsigned rows, unsigned cols)
{
    Matrix result(rows, cols);

    unsigned row = 0;
    unsigned col = 0;
    for(auto& line : input)
    {
        std::stringstream stream(line);
        while(1)
        {
           unsigned isConnectionPossible;
           stream >> isConnectionPossible;
           if(!stream)
              break;
           result.set(row, col, isConnectionPossible);
           col++;
        }
        col = 0;
        row++;
    }


    return result;
}


int main(int argc, char *argv[])
{
    std::cout << "You are at : " << argv[0];
    std::string filename;
    std::cout << "Please give path to configuration file name :)" <<  std::endl;

    getline (std::cin, filename);

    std::ifstream input(filename);

    Data inputData;
    unsigned readingState = 0;
    std::string line;
    std::vector<std::string> buffer;
    while (std::getline(input, line))
    {
        if(line != ".")
        {
            buffer.push_back(line);
        }
        else
        {
            switch(readingState)
            {
            case 0:
                inputData.packets = readPackets(buffer.front());
                break;
            case 1:
                inputData.processors = readProcessorsCapacities(buffer.front());
                break;
            case 2:
                updateProcessorsMaxPackets(buffer.front(), inputData.processors);
                break;
            case 3:
                inputData.connections = readConnections(buffer.front());
                break;
            default:
                inputData.A.push_back(readPossibleConnections(buffer, inputData.packets.size(), inputData.processors.size()));
                break;
            }
            buffer.clear();
            readingState++;
        }

    }
    std::cout << inputData.A.size() << "  " << inputData.connections.size() << "  " << inputData.packets.size() << "  " << inputData.processors.size() << std::endl;


/*
    Data data;

    std::vector<Packet> packets;
    std::vector<Processor> processors;
    std::vector<Connection> connections;
    std::vector<Matrix> A;

    //std::vector<Packet> packets;
    packets.push_back({0, 5});
    packets.push_back({1, 10});
    packets.push_back({2, 10});
    packets.push_back({3, 5});
    packets.push_back({4, 10});
    packets.push_back({5, 15});
    packets.push_back({6, 5});

    //processors
    processors.push_back({0, 50, 0, 0, 2});
    processors.push_back({1, 50, 0, 0, 5});

    //connections
    connections.push_back({10, 0});
    connections.push_back({10, 0});
    connections.push_back({10, 0});
    connections.push_back({15, 0});

    //A's
    Matrix A1(7, 2);
    A1.set(0, 0, 1);
    A1.set(1, 0, 1);
    A1.set(2, 0, 1);
    Matrix A2(7, 2);
    A2.set(0, 1, 1);
    A2.set(1, 1, 1);
    A2.set(2, 1, 1);
    Matrix A3(7, 2);
    A3.set(3, 0, 1);
    A3.set(4, 0, 1);
    A3.set(5, 0, 1);
    A3.set(6, 0, 1);
    Matrix A4(7, 2);
    A4.set(3, 1, 1);
    A4.set(4, 1, 1);
    A4.set(5, 1, 1);
    A4.set(6, 1, 1);


    A1.print();
    std::cout << std::endl;
    A2.print();
    std::cout << std::endl;
    A3.print();
    std::cout << std::endl;
    A4.print();



    A.push_back(A1);
    A.push_back(A2);
    A.push_back(A3);
    A.push_back(A4);

    data.packets = packets;
    data.processors = processors;
    data.connections = connections;
    data.A = A;
*/
    AssignmentProblem problem;
    auto result = problem.solve(inputData);
    std::cout << "RESULT : "<< std::endl;
    result.print();

    return 0;

}
