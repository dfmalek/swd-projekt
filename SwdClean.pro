TEMPLATE = app
CONFIG += console
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    DataReader.cpp \
    AssignmentProblem.cpp \
    Matrix.cpp

HEADERS += \
    Matrix.h \
    DataReader.h \
    Data.h \
    AssignmentProblem.h

